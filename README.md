# NotificationProject

Django API project for sending messages to a remote API service.


## Description
Service for sending messages to a list of clients according to specified rules. 

After creating a new newsletter, if the current time is greater than the start time and less than the end time, all clients that match the filter values specified in this newsletter must be selected from the catalog and sending is started for all these clients.

If a newsletter is created with a start time in the future, the start will be sent automatically upon the onset of this time without any additional actions on the part of the system user.

After a newsletter is created, records are created in the "messages" database table to keep statistics.

Every day at a certain time, an email with statistics is sent. (Item of additional tasks No. 8)

## Models
### Newsletter
- unique id
- date and time of the launch
- text
- filter (tags and operator codes)
- date and time of the end 
### Client
- unique id
- phone number (7XXXXXXXXXX format)
- operator code
- tag
- timezone
### Message
- unique id
- date and time of creation
- sending status
- newsletter id
- client id

***
## Getting started

### Clone remote files

```sh
$ git clone https://gitlab.com/gryroach/NotificationProject.git
$ cd NotificationProject
```
### Create a virtual environment to install dependencies in and activate it:
```sh
$ pip install virtualenv
$ python3 -m venv env
$ source env/bin/activate
```
### Install the dependencies for django project:
```sh
(env)$ pip install -r .web/requirements.txt
```
### Create .env file to configure database settings:
```sh
(env)$ touch .env
(env)$ nano .env
```
### Set the following environment variables:
- ```SECRET_KEY```
- ```DEBUG```
- ```OPEN_API_MESSAGES```
- ```JWT_TOKEN```
- ```POSTGRES_NAME```
- ```POSTGRES_USER```
- ```POSTGRES_PASSWORD```
- ```EMAIL_HOST_USER```
- ```EMAIL_HOST_PASSWORD```
- ```EMAIL_RECIPIENT``` - list of email addresses to send statistics
- ```TIME_SEND_EMAIL``` - mail sending time with statistics (00:00 format)

> **NOTE**: **When developing this project , OPEN_API_MESSAGES is used as https://probe.fbrq.cloud/v1/send/, which requires JWT_TOKEN**

### Run docker-compose 
(Item of additional tasks No. 3)
```sh
(env)$ docker-compose up --build -d
```
### Make migrations for Postgres database
```sh
(env)$ docker-compose exec web python manage.py makemigrations
(env)$ docker-compose exec web python manage.py migrate
(env)$ docker-compose exec web python manage.py notification migrate
```
***
## Usage
### API
- GET -> http://0.0.0.0:8000/api/clients/- get all clients
- POST -> http://0.0.0.0:8000/api/client/add/ - add new client
- GET -> http://0.0.0.0:8000/api/client/<id>/ - get detail information about client with id
- PUT -> http://0.0.0.0:8000/api/client/<id>/ - update client with id
- DELETE -> http://0.0.0.0:8000/api/client/<id>/ - delete client with id
- GET -> http://0.0.0.0:8000/api/newsletters/ - get all newsletters
- POST -> http://0.0.0.0:8000/api/newsletter/add/ - add new newsletter
- GET -> http://0.0.0.0:8000/api/newsletter/<id>/ - get detail information about newsletter with id
- PUT -> http://0.0.0.0:8000/api/newsletter/<id>/ - update newsletter with id
- DELETE -> http://0.0.0.0:8000/api/newsletter/<id>/ - delete newsletter with id
- GET -> http://0.0.0.0:8000/api/newsletters/active/- get all active newsletters

To get a description of the developed API in Swagger UI format, follow the documentation link:
http://0.0.0.0:8000/api/docs/ (Item of additional tasks No. 5)

### Active newsletters
When a newsletters are created and not all messages have been sent yet, the newsletters is active if end time more than current time.

When the start time is less than the current time, attempts are made to send messages to the external service API. 
This happens in a background process and continues until all messages have been sent or until the newsletter end time arrives according to the client's time zone.
(Item of additional tasks No. 9) 

When an active newsletters is deleted, the background process is terminated.
When an active newsletters is changed, the background process is terminated and a new one is created if the conditions for sending messages are met.

***
## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
