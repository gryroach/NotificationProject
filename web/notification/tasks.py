import os

from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
from datetime import datetime
from pytz import timezone
from .src.queries import get_statistic
import json


@shared_task
def statistic_email():
    if datetime.now(timezone(settings.TIME_ZONE)).strftime('%H:%M') == settings.TIME_SEND_EMAIL:
        subject = 'Statistic of newsletters'
        message = json.dumps(get_statistic())
        email_from = settings.EMAIL_HOST_USER
        recipient_list = os.environ.get('EMAIL_RECIPIENT').split()
        try:
            send_mail(subject, message, email_from, recipient_list)
        except Exception as er:
            print(er)
