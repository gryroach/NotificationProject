from pytz import timezone
from django.conf import settings
from datetime import datetime

tz_time = timezone(settings.TIME_ZONE)
now_time = datetime.now(timezone(settings.TIME_ZONE))
current_newsletter_processes = []


def checking_time(instance):
    return instance.finish_datetime > now_time


def start_in_future(start_time):
    return start_time >= now_time


def find_client_in_queryset(instance_id, queryset):
    for instance in queryset:
        if instance.id == instance_id:
            return instance


def finding_alive_procs():
    global current_newsletter_processes
    temp_list = current_newsletter_processes.copy()
    for proc in temp_list:
        if not proc.is_alive():
            current_newsletter_processes.remove(proc)
            print(proc, ' deleted')
    return current_newsletter_processes
