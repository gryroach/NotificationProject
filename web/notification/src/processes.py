import multiprocessing
from .open_api_work import send_to_open_api
from .services import now_time, start_in_future, finding_alive_procs, current_newsletter_processes


def sending_messages_now_process(newsletter, queries, text, finish):
    time_to_start = 0
    if start_in_future(newsletter.start_datetime):
        time_to_start = int((newsletter.start_datetime - now_time).total_seconds())

    process = multiprocessing.Process(target=send_to_open_api, name=f"Newsletter #{newsletter.id}",
                                      args=(text, queries, finish, time_to_start))
    process.daemon = True
    current_newsletter_processes.append(process)
    process.start()


def terminate_newsletter_process(newsletter_id):
    current_newsletter = next((x for x in finding_alive_procs()
                               if str(newsletter_id) == x.name.split()[1].lstrip('#')), None)
    if current_newsletter:
        current_newsletter.terminate()
        print(current_newsletter.name, ' terminated')
