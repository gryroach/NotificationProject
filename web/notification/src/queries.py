from ..models import Newsletter
from ..serializers import NewsletterListSerializer


def get_statistic():
    queryset = Newsletter.objects.all()
    serializer = NewsletterListSerializer(data=queryset, many=True)
    if serializer.is_valid():
        return serializer.validated_data
    return serializer.data
