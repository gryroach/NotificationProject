from pytz import timezone
from django.db.models import Q
from datetime import datetime

from ..models import Client, Message
from .services import checking_time, find_client_in_queryset, now_time, finding_alive_procs
from .processes import sending_messages_now_process, terminate_newsletter_process


def create_new_messages(newsletter, update=False):
    finding_alive_procs()
    if update:
        terminate_newsletter_process(newsletter.id)
    if checking_time(newsletter):
        type_client = newsletter.client_filter.split()
        client_queryset = Client.objects.filter(Q(tag__in=type_client) | Q(operator_code__in=type_client))
        id_client_list = []
        for ins in client_queryset:
            id_client_list.append(ins.id)
        messages = []
        for id_client in id_client_list:
            client = find_client_in_queryset(id_client, client_queryset)
            if newsletter.finish_datetime < datetime.now(timezone(client.timezone)):
                id_client_list.pop(id_client)
                continue
            messages.append(Message(create_datetime=now_time, newsletter=newsletter, client=client))
        Message.objects.bulk_create(messages)
        message_queryset = Message.objects.filter(newsletter=newsletter)
        queries = {"client_set": client_queryset, "message_set": message_queryset}
        sending_messages_now_process(newsletter, queries, newsletter.text, newsletter.finish_datetime)
