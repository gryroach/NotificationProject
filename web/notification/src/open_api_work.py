import requests
import datetime
import json
from pytz import timezone
from django.conf import settings
from random import randint
from time import sleep
from django import db

from .services import find_client_in_queryset


def send_massage(message_id, phone, text):
    url = f"{getattr(settings, 'OPEN_API_MESSAGES')}{message_id}"
    headers = {'Authorization': settings.JWT_TOKEN}
    data = json.dumps({"id": message_id,
                       "phone": phone,
                       "text": text
                       })
    try:
        response = requests.post(url, data=data, headers=headers)
        if response.status_code == 200:
            return True
    except Exception as er:
        print(er)
        return False
    return False


def send_to_open_api(text, queries, finish, time_to_start):
    sleep(time_to_start)
    message_id_list = []
    for ins in queries['message_set']:
        message_id_list.append(ins.id)
    while len(message_id_list) > 0:
        index = randint(0, len(message_id_list)-1)
        message = find_client_in_queryset(message_id_list[index], queries['message_set'])
        client = find_client_in_queryset(message.client.id, queries['client_set'])
        if finish < datetime.datetime.now(timezone(client.timezone)) or message.status == 's':
            message_id_list.pop(message_id_list[index])
        else:
            if send_massage(message_id_list[index], client.phone_number, text):
                db.connections.close_all()
                message.status = 's'
                message.save()
                print(f'Message #{message.id} successfully sent')
                message_id_list.pop(index)
