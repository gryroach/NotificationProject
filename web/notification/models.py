import pytz
from django.db import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from bulk_update_or_create import BulkUpdateOrCreateQuerySet
from django.utils.timezone import now
from .src.services import tz_time


TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))


class Newsletter(models.Model):

    start_datetime = models.DateTimeField("Начало рассылки", null=False)
    text = models.TextField("Текст сообщения", null=False)
    client_filter = models.CharField("Фильтр", max_length=200, null=False)
    finish_datetime = models.DateTimeField("Окончание рассылки", null=False)

    def __str__(self):
        return f"{self.id}: Начало {self.start_datetime.astimezone(tz_time).strftime('%Y/%m/%d %H:%M:%S')}, " \
               f"Окончание {self.finish_datetime.astimezone(tz_time).strftime('%Y/%m/%d %H:%M:%S')}"

    def clean(self):
        if self.finish_datetime < self.start_datetime:
            raise ValidationError("Дата окончания рассылки должна быть после начала")

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    objects = models.Manager()


class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^7\d{10}$', message="Phone number format: '7XXXXXXXXXX'.")
    phone_number = models.CharField(validators=[phone_regex], max_length=11, null=False)
    operator_code = models.CharField("Код оператора", max_length=3, null=False)
    tag = models.CharField("Тэг", max_length=30, blank=True)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')

    def save(self, *args, **kwargs):
        self.operator_code = self.phone_number[1:4]
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.phone_number}"

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    objects = models.Manager()


class Message(models.Model):
    create_datetime = models.DateTimeField("Дата и время создания", default=now)
    newsletter = models.ForeignKey(Newsletter, on_delete=models.SET_NULL, null=True, related_name="messages")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, null=False)
    CHOOSE_STATUS = (
        ('s', 'SUCCESS'),
        ('f', 'FAIL'),
    )

    status = models.CharField('Статус', max_length=1, choices=CHOOSE_STATUS, default='f')

    def __str__(self):
        return f"#{self.id} - {self.client.phone_number} " \
               f"{self.create_datetime.astimezone(tz_time).strftime('%Y/%m/%d %H:%M:%S')} " \
               f"{self.get_status_display()}"

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
        ordering = ['-status']

    objects = BulkUpdateOrCreateQuerySet.as_manager()
