from rest_framework import serializers
from .models import Newsletter, Client, Message
from .src.message_service import create_new_messages


class NewsletterListSerializer(serializers.ModelSerializer):
    start_datetime = serializers.DateTimeField(format='%Y/%m/%d %H:%M:%S')
    finish_datetime = serializers.DateTimeField(format='%Y/%m/%d %H:%M:%S')
    success_messages_count = serializers.SerializerMethodField()
    fail_messages_count = serializers.SerializerMethodField()

    class Meta:
        model = Newsletter
        fields = ("id", "start_datetime", "text", "client_filter", "finish_datetime",
                  "success_messages_count", "fail_messages_count")

    def get_success_messages_count(self, obj):
        return len(Message.objects.filter(newsletter=obj.id).filter(status='s'))

    def get_fail_messages_count(self, obj):
        return len(Message.objects.filter(newsletter=obj.id).filter(status='f'))


class NewsletterDetailSerializer(serializers.ModelSerializer):
    start_datetime = serializers.DateTimeField(format='%Y/%m/%d %H:%M:%S')
    finish_datetime = serializers.DateTimeField(format='%Y/%m/%d %H:%M:%S')
    messages = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Newsletter
        fields = ("id", "start_datetime", "text", "client_filter", "finish_datetime", "messages")

    def validate(self, data):
        if data['start_datetime'] > data['finish_datetime']:
            raise serializers.ValidationError("Дата окончания рассылки должна быть после начала")
        return data

    def create(self, validated_data):
        instance = Newsletter.objects.create(**validated_data)
        create_new_messages(instance)
        return instance

    def update(self, instance, validated_data):
        instance.start_datetime = validated_data.get('start_datetime', instance.start_datetime)
        instance.text = validated_data.get('text', instance.text)
        instance.client_filter = validated_data.get('client_filter', instance.client_filter)
        instance.finish_datetime = validated_data.get('finish_datetime', instance.finish_datetime)
        create_new_messages(instance, update=True)
        return instance


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        read_only_fields = ('operator_code',)
        fields = ('id', 'phone_number', 'operator_code', 'tag', 'timezone')


class MessageSerializer(serializers.ModelSerializer):
    newsletter = NewsletterListSerializer()
    client = ClientSerializer()

    class Meta:
        model = Message
        fields = ('id', 'create_datetime', 'status', 'newsletter', 'client')
