from django.urls import path
from .views import ClientListView, ClientCreateView, ClientDetailView, \
    NewsletterCreateView, NewsletterDetailView, NewsletterListView, NewsletterActiveListView, schema_view

urlpatterns = [
    path("clients/", ClientListView.as_view()),
    path("client/add/", ClientCreateView.as_view()),
    path("client/<int:pk>/", ClientDetailView.as_view()),
    path("newsletter/add/", NewsletterCreateView.as_view()),
    path("newsletter/<int:pk>/", NewsletterDetailView.as_view()),
    path("newsletters/", NewsletterListView.as_view()),
    path("newsletters/active/", NewsletterActiveListView.as_view()),
    path("docs/", schema_view)
]
