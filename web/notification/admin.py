from django.contrib import admin
from .models import Newsletter, Client, Message


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_datetime', 'finish_datetime')


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    readonly_fields = ('operator_code',)
    list_display = ('id', 'phone_number', 'operator_code', 'tag', 'timezone')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'create_datetime', 'status', 'newsletter', 'client')
