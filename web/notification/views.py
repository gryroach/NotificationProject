from rest_framework.generics import CreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework_swagger.views import get_swagger_view

from .models import Newsletter, Client
from .serializers import NewsletterListSerializer, ClientSerializer, NewsletterDetailSerializer
from .src.processes import terminate_newsletter_process
from .src.services import finding_alive_procs


class ClientCreateView(CreateAPIView):

    model = Client
    serializer_class = ClientSerializer


class ClientDetailView(RetrieveUpdateDestroyAPIView):

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class NewsletterCreateView(CreateAPIView):

    model = Newsletter
    serializer_class = NewsletterDetailSerializer


class NewsletterListView(ListAPIView):

    queryset = Newsletter.objects.all()
    serializer_class = NewsletterListSerializer


class NewsletterDetailView(RetrieveUpdateDestroyAPIView):

    queryset = Newsletter.objects.all()
    serializer_class = NewsletterDetailSerializer

    def perform_destroy(self, instance):
        terminate_newsletter_process(instance.id)
        instance.delete()


class NewsletterActiveListView(ListAPIView):

    serializer_class = NewsletterListSerializer

    def get_queryset(self):
        id_active = [int(x.name.split()[1].lstrip('#')) for x in finding_alive_procs()]
        queryset = Newsletter.objects.filter(id__in=id_active)
        return queryset


class ClientListView(ListAPIView):

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


schema_view = get_swagger_view(title='Newsletter API')
